package discordbot.service;

public abstract class Handler {

    private Handler nextHandler;

    protected Handler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public String handle(String request) {
        if (isCorrectRequest(request)) {
            return processHandle(request);
        } else {
            return nextHandler.handle(request);
        }
    }

    protected abstract String processHandle(String request);

    protected abstract boolean isCorrectRequest(String request);
}
