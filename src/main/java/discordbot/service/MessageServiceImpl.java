package discordbot.service;

import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.MessageChannel;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {

    private Handler handler = new ShortCommandHandler();

    @Override
    public void response(Message message) {
        String request = message.getContent();
        final MessageChannel messageChannel = message.getChannel().block();
        if ((request.startsWith("Sebastian, ") || request.startsWith("-"))
                && message.getAuthor().map(user -> !user.isBot()).orElse(false))  {
            String response = handler.handle(request);
            if (response!=null) {
                messageChannel.typeUntil(messageChannel.createMessage(response)).blockLast();
            } else {
                messageChannel.typeUntil(messageChannel.createMessage("Maaf, saya tidak memahami perintah tuan. \n" +
                        "Mungkin tuan bisa cek `Sebastian, help command all` untuk memastikannya")).blockLast();
            }
        }


    }




}
