package discordbot.service;

public class ShortCommandHandler extends Handler {

    private ShortCommandConverter converter;
    public ShortCommandHandler() {
        super(new APIHandler());
        converter = new ShortCommandConverter();
    }

    @Override
    protected String processHandle(String request) {
        return handle(converter.toFullCommand(request));
    }

    @Override
    protected boolean isCorrectRequest(String request) {
        return request.startsWith("-");
    }
}
