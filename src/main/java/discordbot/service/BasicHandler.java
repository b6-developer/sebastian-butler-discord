package discordbot.service;

public class BasicHandler extends Handler {

    public BasicHandler() {
        super(new DefaultHandler());
    }

    @Override
    protected String processHandle(String request) {
        return "Saat ini fitur-fitur sedang dimigrasi dari LINE";
    }

    @Override
    protected boolean isCorrectRequest(String request) {
        return request.contains("Sebastian, help command all");
    }
}
