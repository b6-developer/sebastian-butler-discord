package discordbot.service;

public class ShortCommandConverter {
    public String toFullCommand(String request) {
        String fullRequest = request.replace("-", "Sebastian, ");
        if (request.startsWith("-help")) {
            fullRequest = "Sebastian, help command all";
        } else if (request.startsWith("-syn")) {
            fullRequest = request.replace("-syn", "Sebastian, cari sinonim");
        } else if (request.startsWith("-ant")) {
            fullRequest = request.replace("-ant", "Sebastian, cari antonim");
        } else if (request.startsWith("-m")) {
            fullRequest = request.replace("-m", "Sebastian, cari makna");
        } else if (request.startsWith("-b")) {
            fullRequest = request.replace("-b", "Sebastian, cari kebakuan");
        } else if (request.startsWith("-chs")) {
            fullRequest = request.replace("-chs", "Sebastian, tolong pilih")
                    .replace("-or", "atau");
        } else if (request.startsWith("-w") || request.startsWith("-c")) {
            fullRequest = request.replaceAll("-w|-c", "Sebastian, tolong laporkan-cuaca");
        }  else if (request.startsWith("-tmp")) {
            fullRequest = request.replace("-tmp", "Sebastian, tolong laporkan-suhu");
        } else if (request.startsWith("-h") || request.startsWith("-k")) {
            fullRequest = request.replaceAll("-h|-k", "Sebastian, tolong laporkan-kelembapan");
        } else if (request.startsWith("-t") || request.startsWith("-d")) {
            fullRequest = request.replaceAll("-t|-d", "Sebastian, tolong tentukan");
        } else if (request.startsWith("-roll") || request.startsWith("-r")) {
            fullRequest = request.replaceAll("-roll|-r", "Sebastian, tolong roll");
        }
        return fullRequest.strip();
    }
}
