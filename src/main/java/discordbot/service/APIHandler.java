package discordbot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

public class APIHandler  extends Handler{

    RestTemplate restTemplate;

    public APIHandler() {
        super(new BasicHandler());
        restTemplate = new RestTemplate();
    }

    @Override
    protected String processHandle(String request) {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HashMap<String, String> requestBody = new HashMap<>();
        requestBody.put("request", request);
        ObjectMapper mapper = new ObjectMapper();
        String requestAsString;
        try {
            requestAsString = mapper.writeValueAsString(requestBody);
            HttpEntity<String> entity = new HttpEntity<>(requestAsString, httpHeaders);
            ResponseEntity<String> response = restTemplate.postForEntity(System.getenv("SERVICE_API_URL"), entity, String.class);
            return response.getBody();
        } catch (Exception e) {
            return null;
        }


    }

    @Override
    protected boolean isCorrectRequest(String request) {
        return request.contains("Sebastian,");
    }
}
