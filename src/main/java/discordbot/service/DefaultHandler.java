package discordbot.service;

public class DefaultHandler extends Handler {

    public DefaultHandler() {
        super(null);
    }

    @Override
    protected String processHandle(String request) {
        return "Maaf, saya tidak memahami yang tuan katakan. Saya sedang migrasi dari LINE";
    }

    @Override
    protected boolean isCorrectRequest(String request) {
        return true;
    }
}
