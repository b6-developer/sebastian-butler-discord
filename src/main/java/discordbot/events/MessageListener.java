package discordbot.events;

import discord4j.core.object.entity.Message;
import discordbot.service.MessageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public abstract  class MessageListener {

    @Autowired
    MessageServiceImpl messageServiceImpl;

    public Mono<Void> processCommand(Message eventMessage) {
        return Mono.fromRunnable(() -> messageServiceImpl.response(eventMessage));

    }
}
