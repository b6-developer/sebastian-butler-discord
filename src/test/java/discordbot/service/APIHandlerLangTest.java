package discordbot.service;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class APIHandlerLangTest {
    private final String WORD_NOT_FOUND_MSG = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";
    private Handler apiHandler;

    @Before
    public void setUp() {
        apiHandler = new APIHandler();
    }

    private String buildCommand(String commandType, String word) {
        return "Sebastian, cari " + commandType + " " + word;
    }

    private String buildCariKebakuanResponse(String word, boolean isBaku) {
        String response = "Baik tuan, " + word + " bukanlah kata baku";
        if (isBaku) response = "Baik tuan, " + word + " adalah kata baku";
        return response;
    }

    private String[] getWords(String joinedWords) {
        return joinedWords.split(", ");
    }

    @Test
    public void testCariMaknaValidWord() {
        String word = "hidup";
        String response = apiHandler.handle(buildCommand("makna", word));
        String[] splittedResponse = response.split(word, 2);
        assertNotEquals(WORD_NOT_FOUND_MSG, response);
        assertEquals(2, splittedResponse.length);
        assertEquals("Baik tuan, makna dari kata", splittedResponse[0].strip());
        assertNotNull(splittedResponse[1]);
    }

    @Test
    public void testCariMaknaInvalidWord() {
        assertEquals(WORD_NOT_FOUND_MSG, apiHandler.handle(buildCommand("makna", "suiten")));
    }

    @Test
    public void testCariMaknaFormalWord() {
        String word = "saya";
        assertEquals(buildCariKebakuanResponse(word, true),
                apiHandler.handle(buildCommand("kebakuan", word)));
    }

    @Test
    public void testCariMaknaNonFormalWord() {
        String word = "duarr";
        assertEquals(buildCariKebakuanResponse(word, false),
                apiHandler.handle(buildCommand("kebakuan", word)));
    }

    @Test
    public void testCariSinonimValidWord() {
        String word = "pergi";
        String response = apiHandler.handle(buildCommand("sinonim", word));
        String[] splittedResponse = response.split(word, 2);
        assertNotEquals(WORD_NOT_FOUND_MSG, response);
        assertEquals(2, splittedResponse.length);
        assertEquals("Baik tuan, sinonim dari kata", splittedResponse[0].strip());
        assertTrue(getWords(splittedResponse[1].strip()).length > 0);
    }

    @Test
    public void testCariSinonimInvalidWord() {
        assertEquals(WORD_NOT_FOUND_MSG, apiHandler.handle(buildCommand("sinonim", "bylat")));
    }

    @Test
    public void testCariAntonimValidWord() {
        String word = "masuk";
        String response = apiHandler.handle(buildCommand("antonim", word));
        String[] splittedResponse = response.split(word, 2);
        assertNotEquals(WORD_NOT_FOUND_MSG, response);
        assertEquals(2, splittedResponse.length);
        assertEquals("Baik tuan, antonim dari kata", splittedResponse[0].strip());
        assertTrue(getWords(splittedResponse[1].strip()).length > 0);
    }

    @Test
    public void testCariAntonimInvalidWord() {
        assertEquals(WORD_NOT_FOUND_MSG, apiHandler.handle(buildCommand("antonim", "hayami")));
    }

}