package discordbot.service;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class APIHandlerGeneralTest {
    private final String INVALID_COMMAND_RESPONSE = "maaf, saya tidak bisa memahami yang tuan katakan";
    private Handler apiHandler;

    @Before
    public void setUp() {
        apiHandler = new APIHandler();
    }

    @Test
    public void testInvalidCommand() {
        assertEquals(INVALID_COMMAND_RESPONSE, apiHandler.handle("Sebastian, jeger duar auw"));
    }

}