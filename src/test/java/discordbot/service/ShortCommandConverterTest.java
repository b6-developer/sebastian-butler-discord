package discordbot.service;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ShortCommandConverterTest {
    private ShortCommandConverter converter;

    private String buildChoices(String[] choices, String delimiter) {
        return String.join(delimiter, choices);
    }

    private String buildShortCommand(String commandType, String param) {
        return ("-" + commandType + " " + param).strip();
    }

    private String buildFullCommand(String commandType, String param) {
        return ("Sebastian, " + commandType + " " + param).strip();
    }

    @Before
    public void setUp() {
        converter = new ShortCommandConverter();
    }

    @Test
    public void testCariMakna() {
        String word = "hidup";
        assertEquals(buildFullCommand("cari makna", word),
                converter.toFullCommand(buildShortCommand("m", word)));
    }

    @Test
    public void testCariKebakuan() {
        String word = "saya";
        assertEquals(buildFullCommand("cari kebakuan", word),
                converter.toFullCommand(buildShortCommand("b", word)));
    }

    @Test
    public void testCariSinonim() {
        String word = "pergi";
        assertEquals(buildFullCommand("cari sinonim", word),
                converter.toFullCommand(buildShortCommand("syn", word)));
    }

    @Test
    public void testCariAntonim() {
        String word = "masuk";
        assertEquals(buildFullCommand("cari antonim", word),
                converter.toFullCommand(buildShortCommand("ant", word)));
    }

    @Test
    public void testPilihMultipleChoice() {
        String[] words = {"aku", "dia", "duar"};
        assertEquals(buildFullCommand("tolong pilih", buildChoices(words, " atau ")),
                converter.toFullCommand(buildShortCommand("chs", buildChoices(words, " -or "))));
    }

    @Test
    public void testPilihSingleChoice() {
        String word = "huwala humba";
        assertEquals(buildFullCommand("tolong pilih", word),
                converter.toFullCommand(buildShortCommand("chs", word)));
    }

    @Test
    public void testTentukanWithT() {
        String content = "apa saya kenyang";
        assertEquals(buildFullCommand("tolong tentukan", content),
                converter.toFullCommand(buildShortCommand("t", content)));
    }

    @Test
    public void testTentukanWithD() {
        String content = "apa saya kenyang";
        assertEquals(buildFullCommand("tolong tentukan", content),
                converter.toFullCommand(buildShortCommand("d", content)));
    }

    @Test
    public void testRollWithR() {
        String content = "100";
        assertEquals(buildFullCommand("tolong roll", content),
                converter.toFullCommand(buildShortCommand("r", content)));
    }

    @Test
    public void testRollWithRoll() {
        String content = "100";
        assertEquals(buildFullCommand("tolong roll", content),
                converter.toFullCommand(buildShortCommand("roll", content)));
    }

    @Test
    public void testCuacaWithW() {
        String location = "Jayapura";
        assertEquals(buildFullCommand("tolong laporkan-cuaca", location),
                converter.toFullCommand(buildShortCommand("w", location)));
    }

    @Test
    public void testCuacaWithC() {
        String location = "Jayapura";
        assertEquals(buildFullCommand("tolong laporkan-cuaca", location),
                converter.toFullCommand(buildShortCommand("c", location)));
    }

    @Test
    public void testSuhu() {
        String location = "Penang";
        assertEquals(buildFullCommand("tolong laporkan-suhu", location),
                converter.toFullCommand(buildShortCommand("tmp", location)));
    }

    @Test
    public void testKelembapanWithH() {
        String location = "Bangkok";
        assertEquals(buildFullCommand("tolong laporkan-kelembapan", location),
                converter.toFullCommand(buildShortCommand("h", location)));
    }

    @Test
    public void testKelembapanWithK() {
        String location = "Bangkok";
        assertEquals(buildFullCommand("tolong laporkan-kelembapan", location),
                converter.toFullCommand(buildShortCommand("k", location)));
    }

    @Test
    public void testHelpCommandAll() {
        assertEquals(buildFullCommand("help command all", ""),
                converter.toFullCommand(buildShortCommand("help", "")));
    }

    @Test
    public void testInvalidCommand() {
        String content = "lily nano desu";
        assertEquals(buildFullCommand(content, ""),
                converter.toFullCommand(buildShortCommand(content, "")));
    }
}