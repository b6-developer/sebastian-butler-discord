package discordbot.service;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class APIHandlerButlerTest {
    private String INVALID_ROLL_RESPONSE = "Maaf tuan, saya tidak dapat melakukan perintah tuan";
    private String VALID_ROLL_RESPONSE = "Baik tuan, saya mendapatkan";
    private Handler apiHandler;

    @Before
    public void setUp() {
        apiHandler = new APIHandler();
    }

    private String buildCommand(String commandType, String input) {
        return "Sebastian, tolong " + commandType + " " + input;
    }

    private boolean containsAny(String response, String[] words) {
        boolean isContainAny = false;
        for (String word: words) {
            if (response.contains(word)) {
                isContainAny = true;
                break;
            }
        }
        return isContainAny;
    }

    public String buildLaporanResponse(String reportType, String location) {
        return "Baik tuan, " + reportType + " di " + location + " adalah";
    }

    public String buildFailedLaporanResponse(String reportType) {
        return "Maaf tuan, saya tidak bisa menemukan " + reportType + " untuk kota tersebut";
    }

    @Test
    public void testPilihWithMultipleChoices() {
        String[] choices = {"aku", "dia", "duar"};
        String response = apiHandler.handle(buildCommand("pilih",
                String.join(" atau ", choices)));
        assertTrue(response.contains("Saya memilih"));
        assertTrue(containsAny(response, choices));
    }

    @Test
    public void testPilihWithSingleChoice() {
        String word = "huwala humba";
        String response = apiHandler.handle(buildCommand("pilih", word));
        assertTrue(response.contains("Saya memilih"));
        assertTrue(response.contains(word));
    }

    @Test
    public void testValidTentukan() {
        String response = apiHandler.handle(buildCommand("tentukan", "apa saya kenyang"));
        String[] answers = {"Ya tuan", "Tidak tuan"};
        assertTrue(containsAny(response, answers));
    }

    @Test
    public void testIntegerRoll() {
        String response = apiHandler.handle(buildCommand("roll", "100"));
        assertTrue(response.contains(VALID_ROLL_RESPONSE));
        int rolledNumber = Integer.parseInt(response.split("mendapatkan")[1].strip());
        assertTrue(rolledNumber <= 100 && rolledNumber >= 1);
    }

    @Test
    public void testInvalidRoll() {
        assertEquals(INVALID_ROLL_RESPONSE, apiHandler.handle(buildCommand("roll", "duarr")));
    }

    @Test
    public void testValidLaporkanCuaca() {
        String location = "Jayapura";
        String response = apiHandler.handle(buildCommand("laporkan-cuaca", location));
        assertTrue(response.contains(buildLaporanResponse("cuaca", location)));
        assertNotNull(response.split(location + " adalah")[1].strip());
    }

    @Test
    public void testInvalidLaporkanCuaca() {
        assertEquals(buildFailedLaporanResponse("cuaca"), apiHandler
                .handle(buildCommand("laporkan-cuaca", "Mebu")));
    }

    @Test
    public void testValidLaporkanSuhu() {
        String location = "Penang";
        String response = apiHandler.handle(buildCommand("laporkan-suhu", location));
        assertTrue(response.contains(buildLaporanResponse("suhu", location)));
        assertNotNull(response.split(location + " adalah")[1].strip());
    }

    @Test
    public void testInvalidLaporkanSuhu() {
        assertEquals(buildFailedLaporanResponse("suhu"), apiHandler
                .handle(buildCommand("laporkan-suhu", "Halindom")));
    }

    @Test
    public void testValidLaporkanKelembapan() {
        String location = "Bangkok";
        String response = apiHandler.handle(buildCommand("laporkan-kelembapan", location));
        assertTrue(response.contains(buildLaporanResponse("kelembapan", location)));
        assertNotNull(response.split(location + " adalah")[1].strip());
    }

    @Test
    public void testInvalidLaporkanKelembapan() {
        assertEquals(buildFailedLaporanResponse("kelembapan"), apiHandler
                .handle(buildCommand("laporkan-kelembapan", "Okuu")));
    }
}