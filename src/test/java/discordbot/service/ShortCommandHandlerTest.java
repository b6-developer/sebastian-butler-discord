package discordbot.service;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ShortCommandHandlerTest {
    private final String INVALID_ROLL_RESPONSE = "Maaf tuan, saya tidak dapat melakukan perintah tuan";
    private final String VALID_ROLL_RESPONSE = "Baik tuan, saya mendapatkan";
    private final String INVALID_COMMAND_RESPONSE = "maaf, saya tidak bisa memahami yang tuan katakan";
    private final String WORD_NOT_FOUND_MSG = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";

    private Handler handler;

    private String buildCommand(String commandType, String word) {
        return "-" + commandType + " " + word;
    }

    private String buildChoices(String[] choices) {
        return String.join(" -or ", choices);
    }

    private boolean containsAny(String response, String[] words) {
        boolean isContainAny = false;
        for (String word: words) {
            if (response.contains(word)) {
                isContainAny = true;
                break;
            }
        }
        return isContainAny;
    }

    private String[] getWords(String joinedWords) {
        return joinedWords.split(", ");
    }

    public String buildLaporanResponse(String reportType, String location) {
        return "Baik tuan, " + reportType + " di " + location + " adalah";
    }

    public String buildFailedLaporanResponse(String reportType) {
        return "Maaf tuan, saya tidak bisa menemukan " + reportType + " untuk kota tersebut";
    }

    private String buildCariKebakuanResponse(String word, boolean isBaku) {
        String response = "Baik tuan, " + word + " bukanlah kata baku";
        if (isBaku) response = "Baik tuan, " + word + " adalah kata baku";
        return response;
    }

    @Before
    public void setUp() {
        handler = new ShortCommandHandler();
    }

    @Test
    public void testInvalidCommand() {
        assertEquals(INVALID_COMMAND_RESPONSE, handler.handle("-lily nano desu"));
    }

    @Test
    public void testCariMaknaValidWord() {
        String word = "hidup";
        String response = handler.handle(buildCommand("m", word));
        String[] splittedResponse = response.split(word, 2);
        assertNotEquals(WORD_NOT_FOUND_MSG, response);
        assertEquals(2, splittedResponse.length);
        assertEquals("Baik tuan, makna dari kata", splittedResponse[0].strip());
        assertNotNull(splittedResponse[1]);
    }

    @Test
    public void testCariMaknaInvalidWord() {
        assertEquals(WORD_NOT_FOUND_MSG, handler.handle(buildCommand("m", "suiten")));
    }

    @Test
    public void testCariKebakuanFormalWord() {
        String word = "saya";
        assertEquals(buildCariKebakuanResponse(word, true),
                handler.handle(buildCommand("b", word)));
    }

    @Test
    public void testCariKebakuanNonFormalWord() {
        String word = "duarr";
        assertEquals(buildCariKebakuanResponse(word, false),
                handler.handle(buildCommand("b", word)));
    }

    @Test
    public void testCariSinonimValidWord() {
        String word = "pergi";
        String response = handler.handle(buildCommand("syn", word));
        String[] splittedResponse = response.split(word, 2);
        assertNotEquals(WORD_NOT_FOUND_MSG, response);
        assertEquals(2, splittedResponse.length);
        assertEquals("Baik tuan, sinonim dari kata", splittedResponse[0].strip());
        assertTrue(getWords(splittedResponse[1].strip()).length > 0);
    }

    @Test
    public void testCariSinonimInvalidWord() {
        assertEquals(WORD_NOT_FOUND_MSG, handler.handle(buildCommand("syn", "bylat")));
    }

    @Test
    public void testCariAntonimValidWord() {
        String word = "masuk";
        String response = handler.handle(buildCommand("ant", word));
        String[] splittedResponse = response.split(word, 2);
        assertNotEquals(WORD_NOT_FOUND_MSG, response);
        assertEquals(2, splittedResponse.length);
        assertEquals("Baik tuan, antonim dari kata", splittedResponse[0].strip());
        assertTrue(getWords(splittedResponse[1].strip()).length > 0);
    }

    @Test
    public void testCariAntonimInvalidWord() {
        assertEquals(WORD_NOT_FOUND_MSG, handler.handle(buildCommand("ant", "hayami")));
    }

    @Test
    public void testPilihWithMultipleChoices() {
        String[] choices = {"aku", "dia", "duar"};
        String response = handler.handle(buildCommand("chs",
                buildChoices(choices)));
        assertTrue(response.contains("Saya memilih"));
        assertTrue(containsAny(response, choices));
    }

    @Test
    public void testPilihWithSingleChoice() {
        String word = "huwala humba";
        String response = handler.handle(buildCommand("chs", word));
        assertTrue(response.contains("Saya memilih"));
        assertTrue(response.contains(word));
    }

    @Test
    public void testValidTentukan() {
        String response = handler.handle(buildCommand("t", "apa saya kenyang"));
        String[] answers = {"Ya tuan", "Tidak tuan"};
        assertTrue(containsAny(response, answers));
    }

    @Test
    public void testIntegerRoll() {
        String response = handler.handle(buildCommand("r", "100"));
        assertTrue(response.contains(VALID_ROLL_RESPONSE));
        int rolledNumber = Integer.parseInt(response.split("mendapatkan")[1].strip());
        assertTrue(rolledNumber <= 100 && rolledNumber >= 1);
    }

    @Test
    public void testInvalidRoll() {
        assertEquals(INVALID_ROLL_RESPONSE, handler.handle(buildCommand("r", "duarr")));
    }

    @Test
    public void testValidLaporkanCuaca() {
        String location = "Jayapura";
        String response = handler.handle(buildCommand("c", location));
        assertTrue(response.contains(buildLaporanResponse("cuaca", location)));
        assertNotNull(response.split(location + " adalah")[1].strip());
    }

    @Test
    public void testInvalidLaporkanCuaca() {
        assertEquals(buildFailedLaporanResponse("cuaca"), handler
                .handle(buildCommand("c", "Mebu")));
    }

    @Test
    public void testValidLaporkanSuhu() {
        String location = "Penang";
        String response = handler.handle(buildCommand("tmp", location));
        assertTrue(response.contains(buildLaporanResponse("suhu", location)));
        assertNotNull(response.split(location + " adalah")[1].strip());
    }

    @Test
    public void testInvalidLaporkanSuhu() {
        assertEquals(buildFailedLaporanResponse("suhu"), handler
                .handle(buildCommand("tmp", "Halindom")));
    }

    @Test
    public void testValidLaporkanKelembapan() {
        String location = "Bangkok";
        String response = handler.handle(buildCommand("h", location));
        assertTrue(response.contains(buildLaporanResponse("kelembapan", location)));
        assertNotNull(response.split(location + " adalah")[1].strip());
    }

    @Test
    public void testInvalidLaporkanKelembapan() {
        assertEquals(buildFailedLaporanResponse("kelembapan"), handler
                .handle(buildCommand("h", "Okuu")));
    }

    @Test
    public void testHelpAll() {
        String response = handler.handle(buildCommand("help", ""));
        assertTrue(response.contains("Baik tuan, perintah yang tersedia adalah"));
        assertTrue(response.contains("Bantuan  tata bahasa"));
        assertTrue(response.contains("Bantuan butler"));
    }
}